﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MoveArrow : MonoBehaviour {

    public float speed = 30f;
    public float deathtimer = 2f;
	// Update is called once per frame
	void Update () {
        Vector3 pos = transform.position;
        pos.x -= speed * Time.deltaTime;
        transform.position = pos;
        deathtimer -= Time.deltaTime;
        if(deathtimer <= 0)
        {
            Destroy(gameObject);
            deathtimer = 2f;
        }
	}

    private void OnTriggerEnter2D(Collider2D col)
    {
        Destroy(gameObject);
    }
}
