﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpawnScript : MonoBehaviour {

    public GameObject arrow;
    public float spawnRate = 3f;

	// Update is called once per frame
	void Update () {
        spawnRate -= Time.deltaTime;
        if (spawnRate <= 0)
        {
            Instantiate(arrow, gameObject.transform.position, Quaternion.identity);
            spawnRate = 3f;
        }
	}
}
