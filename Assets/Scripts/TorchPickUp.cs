﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TorchPickUp : MonoBehaviour
{

    public GameObject player;
    private bool hasTorch = false;
    private float torchTimer = 5f;
    public Light light;

    

    private void OnTriggerStay2D(Collider2D collision)
    {
        if (collision.gameObject.tag == "Player")
        {
            if (Input.GetKeyDown(KeyCode.E))
            {
                hasTorch = true;
            }       
        }
    }

    private void Update()
    {
        if (hasTorch)
        {
            torchTimer -= Time.deltaTime;
            gameObject.transform.position = player.transform.position;
        }

        if (torchTimer <= 0)
        {
            hasTorch = false;
            light.enabled = false;
            torchTimer = 5f;
            Destroy(gameObject);
        }
    }
}