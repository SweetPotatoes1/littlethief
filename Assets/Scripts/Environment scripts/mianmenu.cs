﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class mianmenu : MonoBehaviour {

    public void PlayGame()
    {
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex + 1); // loads up the main game just need to add the game to the scenes 
    }

    public void QuitGame()
    {
        Debug.Log("Quit"); //will tell you in the log when you quit the game so you dont have to run the game to see if the button is working.
        Application.Quit();

    }


}
