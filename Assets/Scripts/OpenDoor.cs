﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
public class OpenDoor : MonoBehaviour
{

    public Animator animator;
    int sceneNr;
    public bool commutable = true;
    public bool YouWinIfYouGoThroughThisDoor = false;
    public static bool YouWin = false;
    

    private void Start()
    {
        sceneNr = SceneManager.GetActiveScene().buildIndex;      
    }
    
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.tag == "Player")
        {
            animator.SetBool("NearbyDoor",true);
        
        }
    }
   

    private void OnTriggerStay2D(Collider2D collision)
    {
        if (collision.gameObject.tag == "Player")
        {
            if (Input.GetKeyDown(KeyCode.E) && !YouWinIfYouGoThroughThisDoor)
            {
                SceneManager.LoadScene(sceneNr + 1);
            }

            if (Input.GetKeyDown(KeyCode.E) && commutable && !YouWinIfYouGoThroughThisDoor)
            {
                SceneManager.LoadScene(sceneNr - 1);
            }
            if (Input.GetKeyDown(KeyCode.E) && YouWinIfYouGoThroughThisDoor)
            {
                YouWin = true;
            }
        }
    }

    private void OnTriggerExit2D(Collider2D collision)
    {
        if (collision.gameObject.tag == "Player")
        {
            animator.SetBool("NearbyDoor", false);
           
        }
    }

    
}
   

