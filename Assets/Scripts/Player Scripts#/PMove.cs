﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PMove : MonoBehaviour {
    public CharacterController2D controller;
    private float horizontalMove;
    public float speed = 30f;
    private bool jump = false;
    private bool crouch = false;
    public static bool stolen = false;
    public Rigidbody2D rb;
    private bool onLadder = false;
    public float ClimbSpeed = 5f;
    float vertical;
    public AudioManager audiomanager;
    public Animator animator;

    private void Start()
    {
        Time.timeScale = 1f;
        OpenDoor.YouWin = false;
    }
    void Update () {
        float vertical = Input.GetAxis("Vertical");
        if (onLadder)
        {
            rb.gravityScale = 0;
            rb.velocity = new Vector2(rb.velocity.x, vertical * ClimbSpeed);
           
           /* if (Input.GetKey(KeyCode.W))
            {
                rb.velocity = Vector2.up * ClimbSpeed;
            }
            if (Input.GetKey(KeyCode.S))
            {
                rb.velocity = Vector2.down * ClimbSpeed;
            }
           */ if(Input.GetButtonDown("Jump"))
            {
                onLadder = false;
            }
           
        }
        else
        {

            horizontalMove = Input.GetAxisRaw("Horizontal") * speed;
            animator.SetFloat("speed", Mathf.Abs(horizontalMove));
            if (CharacterController2D.m_Grounded)
            {
                if (Input.GetButtonDown("Jump"))
                {
                    audiomanager.Play("jump");
                    jump = true;
                    animator.SetBool("isJumping", true);
                    

                }
            }
            if (Input.GetButtonDown("crouch"))
            {
                crouch = true;
            }
            else if (Input.GetButtonUp("crouch") && !jump)
            {
                crouch = false;
            }
        }
       
    }

    public void onLand()
    {
        animator.SetBool("isJumping", false);
    }

    private void OnTriggerStay2D(Collider2D col)
    {
        if (col.gameObject.tag == "CanClimb")
        {
            if (Input.GetKeyDown(KeyCode.E))
            {
                onLadder = true;
                gameObject.transform.position = new Vector3(col.transform.position.x, gameObject.transform.position.y, gameObject.transform.position.z);
            }
        }
        if (col.gameObject.layer == 9)
        {
            if (Input.GetKeyDown(KeyCode.E))
            {
                audiomanager.Play("pickup");
                animator.SetBool("isStealing", true);
                Invoke("stealanim", 0.75f);
            }
        }
    }

    private void stealanim()
    {
        animator.SetBool("isStealing", false);
    }
    private void OnTriggerExit2D(Collider2D col)
    {
        if (col.gameObject.tag == "CanClimb")
        {
            onLadder = false;
            rb.gravityScale = 5;
        }
    }

    private void FixedUpdate()
    {
        controller.Move(horizontalMove * Time.fixedDeltaTime, crouch, jump);
        jump = false;
        
    }
}
