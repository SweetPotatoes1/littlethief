﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Death : MonoBehaviour {

    public int health = 3;
    public GameObject heart1;
    public GameObject heart2;
    public GameObject heart3;
    public GameObject DeathMenuUI;
    public GameObject WinMenuUI;
    private bool hasTakenDmg = false;
    public AudioManager audiomanager;
    // Update is called once per frame
    private void Start()
    {
        health = 3;
    }
    void Update() {
        if (hasTakenDmg)
        {
            health--;
            hasTakenDmg = false;

        }
        if(OpenDoor.YouWin)
        {
            WinMenuUI.SetActive(true);
            Time.timeScale = 0f;
        }
        if (health == 0)
        {
            heart1.SetActive(false);
            audiomanager.Play("DeathSound");
            DeathMenu();
        }

        if (health == 3)
        {
            heart1.SetActive(true);
            heart2.SetActive(true);
            heart3.SetActive(true);
        }
        if (health == 2)
        {

            heart3.SetActive(false);
        }
        if (health == 1)
        {
            heart2.SetActive(false);
        }
    }
    private void OnTriggerEnter2D(Collider2D col)
    {
        if (col.gameObject.tag == "DealsDmg")
        {
            audiomanager.Play("DeathSound");
            hasTakenDmg = true;
        }

    }
    void DeathMenu()
    {
        DeathMenuUI.SetActive(true);
        Time.timeScale = 0f;
    }

    

    public void RestartGame()
    {
        SceneManager.LoadScene(1);
        Time.timeScale = 1f;
    }
}
